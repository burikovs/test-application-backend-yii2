<?php

namespace app\models;

use app\common\helpers\ActiveStatus;
use Throwable;
use yii\caching\DbDependency;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

class BaseModel extends ActiveRecord
{
    public const DEFAULT_CACHE_DURATION = 86400;

    /**
     * @param bool $map
     * @param array $attributes
     * @param null $where
     * @param null $orderBy
     * @return array|mixed|ActiveRecord
     * @throws Throwable
     */
    public static function getList(array $attributes = [], $where = null, $orderBy = null)
    {
        $defaultAttributes = ['type', 'document_template_id'];
        if (empty($attributes)) {
            $attributes = $defaultAttributes;
        }
        $models = static::getDb()->cache(
            function () use ($where, $orderBy, $attributes) {
                $query = static::find()->select($attributes)->indexBy('document_template_id');

                if ($where) {
                    $query->where($where);
                }

                if ($orderBy) {
                    $query->orderBy($orderBy);
                }

                return $query->column();
            },
            static::DEFAULT_CACHE_DURATION,
//            static::getDbDependency()
        );

        return $models;
    }

    /**
     * @param bool $map
     * @param array $attributes
     * @param null $where
     * @param null $orderBy
     * @return array|mixed|ActiveRecord
     * @throws Throwable
     */
    public static function getActiveList(array $attributes = [], $where = null, $orderBy = null)
    {
        $defaultAttributes = ['name', 'id'];
        if (empty($attributes)) {
            $attributes = $defaultAttributes;
        }
        $models = static::getDb()->cache(
            function () use ($where, $orderBy, $attributes) {
                $query = static::find()->select($attributes)->indexBy('id');

                if ($where) {
                    $query->where($where);
                }

                $query->andWhere(['status' => ActiveStatus::ACTIVE]);

                if ($orderBy) {
                    $query->orderBy($orderBy);
                }

                return $query->column();
            },
            static::DEFAULT_CACHE_DURATION,
//            static::getDbDependency()
        );

        return $models;
    }

    /**
     * @param null $table
     * @param string $field
     *
     * @return DbDependency
     */
    public static function getDbDependency($table = null, string $field = 'updated_at'): DbDependency
    {
        if ($table == null) {
            $table = self::tableName();
        }

        $dependency = new DbDependency();
        $dependency->sql = 'SELECT MAX(' . $field . ') FROM ' . $table;
        return $dependency;
    }


    public function cleanHtml($value): string
    {
        $value = Html::encode($value);
        return str_replace('amp;', '', HtmlPurifier::process($value));
    }

}
